const usersController = new Object();
var userSchema = require('../collections/userModel')
const jwt = require('jsonwebtoken');
let cacheDb = require('../dbConnection')
var env = require('dotenv');
env.config();

usersController.registerUser = async(req, res) => {
    try{
        if(!req.body){
            return res.status(400).send({ status: false, message: "invalid input"}); 
        }       
    let data = req.body;
    var userdata = new userSchema({
        name: data.name,
        email: data.email,
        password: data.password
    });
   let result = await userdata.save();
   if(result){
    return res.status(200).send({ status: true, message: "saved successfully"});
   }
    }catch(err){
        return res.status(500).send({ status: false, message: err.message}); 
    }

}

usersController.login = async (req, res) => {
    try {
        let data = req.body;
        let findUser = await userSchema.findOne({ email: data.email }, { password: data.password });
        if (findUser) {
            let token = await jwt.sign({ userId: findUser._id }, process.env.secret, { expiresIn: "1d" });
            if (token) {
                await cacheDb.cache.set(token,process.env.expiry);
                return res.status(200).send({ status: true, message: "login successful", data: token });
            }
            return res.status(400).send({ status: false, message: "login failed" });
        }
        return res.status(400).send({ status: false, message: "email or password invalid" });
    } catch (err) {
        return res.status(500).send({ status: false, message: err.message ? err.message : err })
    }
}

usersController.getUser = async (req, res) => {
    try {
        if (!req.headers.token) {
            return res.status(403).send({ status: false, message: 'token not provided in headers' });
        }
        const token = req.headers.token
        let decode = jwt.decode(token);
        if (!decode) {
            return res.status(403).send({ status: false, message: 'provided token is invalid' });
        }
        let userId = decode.userId;
        let secret = process.env.secret;
        let verifytoken = await jwt.verify(token, secret);
        if (verifytoken) {
            let findUser = await userSchema.findOne({ _id: userId });
            if (findUser) {
                return res.status(200).send({ status: true, message: "success", data: findUser });
            }
            return res.status(400).send({ status: false, message: "failed", data: findUser });
        }
        return res.status(403).send({ status: false, message: "token verification failed", data: verifytoken });
    } catch (error) {
        return res.status(500).send({ error: false, message: error.message });
    }
}

module.exports = usersController