let redis = require('redis')
const env = require('dotenv')
env.config()

let cache = redis.createClient({ host: process.env.REDIS, port: 6379 })
cache.on('connect', function () {
  console.log('Redis client connected')
})

module.exports = {cache};