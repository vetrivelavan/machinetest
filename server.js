const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
var env = require('dotenv');
env.config();
let router = require('./routes/mainRoute');
app.use(router)
var mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/machineTest")
const port = process.env.port;
app.listen(port, ()=>
   console.log(`testing port ${port}`))
