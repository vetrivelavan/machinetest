var express = require('express');
router = express.Router();
var users = require('../controller/userController')

router.route('/users')
    .post(function (req, res) {
        users.registerUser(req, res)
    })
    .get(function (req, res) {
        users.getUser(req, res)
    })
    
router.route('/login')
    .post(function (req, res) {
        users.login(req, res)
    })

    
module.exports = router;